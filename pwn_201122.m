%% solve equation 
figure;
% everything is measured in Kuhn lengths
k = 1.75; % Kunh length is 1.75 nm
v = 0.59 % Flory exponent

% conversion factor
conv = k/0.38; % 0.38 nm/a.a.
c0 = [0 1 0];
c1 = [0 0 1];

% make Fig. S1a
figure;
n = 0;
ws = (10:2:40)/k; % cell wall width in nm
R = 3/k; % pore radius is nm
nw = numel(ws);
for w=ws
  color = c0+(c1-c0)*n/(nw-1);
  p = 1/k/2*(5:0.1:20); % divide periplasmic thickness by 2
  plot(k*p*2,conv*(p.^(1/v)*(1+w/R)),'Color',color);
  hold on;
  n=n+1;
end
ylim([0 1200])
print('-dpdf','figS1a_201123.pdf');

%%
figure;
Rs = 2:0.2:4.2; % pore radii in nm
Rs = Rs/k;
w = 20/k; % width at 20 nm
n = 0;
nR = numel(Rs);
for R=Rs
  color = c0+(c1-c0)*n/(nR-1);
  p = 1/k/2*(5:0.1:20); % periplasmic thickness in nm
  plot(k*p*2,conv*(p.^(1/v)*(1+w/R)),'Color',color);

  hold on;
  n=n+1;
end
ylim([0 1200])
print('-dpdf','figS1b_201123.pdf');

%%
figure;
p = 12/k/2; % set periplasmic thickness at 12 nm
Rs = 2:0.2:4.2; % pore radii in nm
Rs = Rs/k;
n = 0;
nR = numel(Rs);
for R=Rs
  color = c0+(c1-c0)*n/(nR-1);
  w = 1/k*[0.1:0.1:40]; 
  plot(k*w,conv*(p.^(1/v)*(1+w/R)),'Color',color);
  hold on;
  n=n+1;
end
ylim([0 1200])
print('-dpdf','figS1c_201123.pdf');
