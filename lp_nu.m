L = 228;
c = 0;
fac = 1.58
for nu=0.5:0.01:0.6
    c = c+1;
    n(c) = nu;
    lp(c) = 0.5*(sqrt(6)*8*fac)^(1/(1-nu))/L^(nu/(1-nu));
end
figure;plot(n,lp);

%%
N = 600;
a1 = 0.0216;
a2 = 0.406;
a3 = 0.0821;
Rh = 8;
x1 = 1/Rh-a1/(N^0.6-N^0.33);
x2 = a3-a1*a2*N^0.33/(N^0.6-N^0.33);
Rg = x2/x1;
