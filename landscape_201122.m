% arguments to pass are the periplasm and cell wall thickness
function data = landscape_201122(p,w,R,nu,conv) % conv is conversion factor from Kuhn lengths to aa

% length of polymer
nmax = 300;
n = 1:nmax;

Fpc = 1/p^(1/nu)*n;
Fps = p^(1/(1-nu))./n.^(nu/(1-nu));

Fwc = 1/R^(1/nu)*n;
Fws = w^(1/(1-nu))./n.^(nu/(1-nu));

% plot the value of Fpc for all n
%plot(n,Fpc);

% loop over all values of np and nw

% set nw to be the minimum 
nwmin = R^((1-nu)/nu)*w;
Fwsmin = w/R;

% crossover is at p^2
npmin = p^(1/nu);

Fp(1:round(npmin)) = Fps(1:round(npmin));
Fp(round(npmin)+1:nmax) = Fpc(round(npmin)+1:nmax);

% crossover is at R^2
Fw(1:round(nwmin)) = Fws(1:round(nwmin));
Fw(round(nwmin)+1:nmax) = Fwc(round(nwmin)+1:nmax);

for k=1:50 % number in wall
  for j=k+1:nmax
    Fwp(k,j) = Fw(k)+Fp(j-k);
  end
end
  
hold on;
plot(n*conv,Fp,'Color',[1 0.5 0.5]);
plot(n*conv,Fw,'y');

c0 = [1 0 0];
c1 = [0 0 1];
ck = 0;
for k=5:5:50
  c = c0+(c1-c0)*ck/9;
  plot(n(k+1:end)*conv,Fwp(k,k+1:end),'Color',c);
  ck = ck+1;
end

pause;
ylim([0 60])
xlim([0 300])
ck = 0;
disp('start');
for k=12:1:21
  c = c0+(c1-c0)*ck/9;
  disp(k);
  plot(n(k+1:end)*conv,Fwp(k,k+1:end),'Color',c);
  ck = ck+1;
  pause;
end

% total energy for extending outside the cell wall
Fmin = 1+Fwsmin;
plot(n*conv,Fmin*ones(size(n)),'c');
disp(p^2+nwmin);



hold off;
% for p=12,w=20,R=3, k=17
% for 12,30,3 k=25
% for 10,20,3 17
%% compute the fraction that is outside the wall for a particular value of k
k = 17; 
Next = n(k+1:end);
Eext = Fwp(k,k+1:end);

Np = n;
Ep = Fp;

data.Next = Next;
data.Eext = Eext;
data.Np = n;
data.Ep = Ep;

